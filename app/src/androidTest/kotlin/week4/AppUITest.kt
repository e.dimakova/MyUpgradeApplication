package week4

import week4.screens.FirstPage.Companion.FIRST_TEXT
import week4.screens.FirstPage.Companion.firstPage
import week4.SecondPage.Companion.LOGIN_HINT
import week4.SecondPage.Companion.PASSWORD_HINT
import week4.SecondPage.Companion.TEST_LOGIN
import week4.SecondPage.Companion.TEST_PASSWORD
import week4.SecondPage.Companion.secondPage
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.kaspersky.kaspresso.internal.systemscreen.NotificationsFullScreen.pressBack
import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import ru.tinkoff.myupgradeapplication.MainActivity

@RunWith(AndroidJUnit4::class)
class AppUITest {

    private val delay = 1000L

    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun checkShowPasswordError() {
        firstPage {
            pressNextButton()
        }
        secondPage {
            enterLogin(TEST_LOGIN)
            pressSubmitButton()
            assert(checkText("Password field must be filled!"))
        }
    }

    @Test
    fun checkShowLoginError() {
        firstPage {
            pressNextButton()
        }
        secondPage {
            enterPassword(TEST_PASSWORD)
            pressSubmitButton()
            assert(checkText("Login field must be filled!"))
        }
    }

    @Test
    fun checkShowAllFieldsEmptyError() {
        firstPage {
            pressNextButton()
        }
        secondPage {
            pressSubmitButton()
            assert(checkText("Both of fields must be filled!"))
        }
    }

    @Test
    fun checkShowDialogAfterDialogButton() {
        firstPage {
            pressDialogButton()
            waiting(delay)
            assert(titleDialog.exists())
            assert(messageDialog.exists())
        }
    }

    @Test
    fun checkNoShowDialog() {
        firstPage {
            pressDialogButton()
            waiting(delay)
            pressBack()
            waiting(delay)
            assertFalse(titleDialog.exists())
            assertFalse(messageDialog.exists())
        }
    }

    @Test
    fun checkFirstText_AfterBackToPage() {
        firstPage {
            pressChangeButton()
            pressNextButton()
        }
        secondPage {
            pressPreviousButton()
        }
        firstPage {
            assert(checkText(FIRST_TEXT))
        }
    }

    @Test
    fun checkNoLoginNoPasswordView() {
        firstPage {
            pressNextButton()
        }
        secondPage {
            enterLogin(TEST_LOGIN)
            enterPassword(TEST_PASSWORD)
            pressPreviousButton()
        }
        firstPage {
            pressNextButton()
        }
        secondPage {
            assertEquals(LOGIN_HINT, loginFieldObject.text)
            assertEquals(PASSWORD_HINT, passwordFieldObject.text)
        }
    }
}